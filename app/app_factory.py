from fastapi import APIRouter, FastAPI

from routers import router as dish_router


def create_app() -> FastAPI:
    routers: tuple = (dish_router,)
    app: "FastAPI" = FastAPI()
    router: "APIRouter" = APIRouter(prefix="/api")
    [router.include_router(app_router) for app_router in routers]
    app.include_router(router)
    return app
