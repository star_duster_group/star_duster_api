class AlreadyExists(Exception):
    msg = "Instance already exists"
