import logging

from sqlalchemy import Column, Integer, String

from database import Base

logger = logging.getLogger(__name__)


class Dish(Base):
    __tablename__ = "dish"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, unique=True, nullable=False)

    __table_args__ = ({"extend_existing": True},)
