from pydantic import BaseModel


class DishModelIn(BaseModel):
    name: str


class DishModelOut(DishModelIn):
    id: int

    class Config:
        orm_mode = True
