import os
from logging import config

FORMAT: str = "%(levelname)s | %(name)s | " "%(asctime)s | %(lineno)d | %(message)s"
LOGGER_LEVEL: int = int(os.environ.get("LOGGER_LEVEL", 20))

dict_config: dict = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {"base": {"format": FORMAT}},
    "handlers": {
        "file": {
            "class": "logger.LevelFileHandler",
            "level": LOGGER_LEVEL,
            "formatter": "base",
            "when": "d",
            "interval": 1,
        },
        "stream": {
            "class": "logging.StreamHandler",
            "level": LOGGER_LEVEL,
            "formatter": "base",
        },
    },
    "loggers": {
        "": {
            "level": LOGGER_LEVEL,
            "handlers": ["file", "stream"],
            "propagate": False,
        }
    },
}

config.dictConfig(dict_config)
