import logging

import uvicorn

from app_factory import create_app
from database import async_session, engine, generate_db

app = create_app()

logger = logging.getLogger(__name__)


@app.on_event("startup")
async def startup() -> None:
    await generate_db()


@app.on_event("shutdown")
async def shutdown():
    async with async_session() as session:
        await session.close()
    await engine.dispose()


if __name__ == "__main__":
    uvicorn.run("main:app", port=5001)
