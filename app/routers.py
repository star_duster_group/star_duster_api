import logging

from fastapi import APIRouter, HTTPException, status
from sqlalchemy.exc import IntegrityError

from dals import DishDAL
from database import async_session
from exceptions import AlreadyExists
from schemas import DishModelIn, DishModelOut

router = APIRouter(prefix="/dish")

logger = logging.getLogger(__name__)


@router.post("/add", response_model=DishModelIn)
async def add_dish(dish: DishModelIn) -> DishModelIn:
    try:
        dal: "DishDAL" = DishDAL(async_session)
        await dal.check(dish.name)
        return await dal.create(dish)
    except (IntegrityError, AlreadyExists):
        raise HTTPException(status.HTTP_409_CONFLICT, "Already exists!")


@router.get("/list", response_model=list[DishModelOut])
async def _list() -> list[DishModelOut]:
    dal: "DishDAL" = DishDAL(async_session)
    return await dal.all()


@router.get("/{dish_id}", response_model=DishModelOut)
async def get(dish_id: int) -> DishModelOut:
    dal: "DishDAL" = DishDAL(async_session)
    return await dal.get_by_id(dish_id)


@router.delete("/{dish_id}", response_model=DishModelOut)
async def _delete(dish_id: int) -> DishModelOut:
    dal: "DishDAL" = DishDAL(async_session)
    dish: DishModelOut = await dal.get_by_id(dish_id)
    await dal.delete(dish_id)
    return dish
