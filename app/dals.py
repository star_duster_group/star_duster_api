import logging
from typing import Type

from fuzzywuzzy import fuzz
from sqlalchemy import delete
from sqlalchemy.future import select
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import Select

from exceptions import AlreadyExists
from models import Dish
from schemas import DishModelIn

logger = logging.getLogger(__name__)

MATCH_RATE: int = 60


class DishDAL:
    model: Type[Dish] = Dish

    def __init__(self, db_session: sessionmaker) -> None:
        self.session = db_session

    async def all(self) -> list["Dish"]:
        async with self.session() as session:
            async with session.begin():
                query: "Select" = select(self.model)
                result = await session.execute(query)
        return result.scalars().all()

    async def create(self, schema: DishModelIn) -> DishModelIn:
        async with self.session() as session:
            async with session.begin():
                new_dish: "Dish" = self.model(**schema.dict())
                session.add(new_dish)
        return schema

    async def get_by_id(self, dish_id: int) -> Dish:
        async with self.session() as session:
            async with session.begin():
                query: "Select" = select(self.model).where(self.model.id == dish_id)
                result = await session.execute(query)
                if result := result.one_or_none():
                    (result,) = result
        return result

    async def delete(self, dish_id: int) -> None:
        async with self.session() as session:
            async with session.begin():
                query = delete(self.model).where(self.model.id == dish_id)
                await session.execute(query)
                await session.commit()

    async def check(self, dish_name: str) -> None:
        async with self.session() as session:
            async with session.begin():
                query: "Select" = select(self.model.name)
                result = await session.execute(query)
        dish_names = result.scalars().all()
        for word in dish_names:
            rate = fuzz.ratio(dish_name, word)
            logger.info(f"Got {rate=}: for '{word}'")
            if rate > MATCH_RATE:
                raise AlreadyExists
